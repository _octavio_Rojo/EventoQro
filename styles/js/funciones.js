var xmlhttp;
//compruba el navegador que se usa
function ComprobarNavegador(){
    var xmlhttp1;
    if(window.XMLHttpRequest){
       xmlhttp1 = new XMLHttpRequest();
        return xmlhttp1;
       } else {
           xmlhttp1 = new ActiveXObject("Microsoft.XMLHTTP");
           return xmlhttp1;
       }
}
//verifica que sea un correo correcto
function VerificarCorreo(correo){
    var reg=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
    var correcto;
    if(reg.test(correo)){
        correcto= true;
        return correcto;
    } else{
        correcto=false;
    }
}
//funcion donde solo acepta números
function solonumeros(e){
        key=e.keyCode || e.which;
        teclado=String.fromCharCode(key);
        numero="0123456789";
        especiales="8-37-38-46";
        teclado_especial=false;
        
        for(var i in especiales){
            if(key==especiales[i]){
                teclado_especial=true;
            }
        }
        
        if(numero.indexOf(teclado)==-1 && !teclado_especial){
            return false;
        }
    BorrarValidacion();
}
//Borrar validación
function BorrarValidacion(){
    document.getElementById("mensaje").innerHTML="";
}
//Funcion para registrar el folio
function validarFolio(){
    var bandera = $("#bandera").val();
    if(bandera==1){
        var folioActual  = $("#folio").val();
        var folioAnterior= $("#replicaFolio").val();
        var parametros ={
            "folioActual" : folioActual,
            "folioAnterior" : folioAnterior
        }
        if(folioActual!=''){
            $.ajax({
                type: 'POST',
                data: parametros,
                url : "../models/actualizarFolio.php",
                success: function(data){
                    if(data.indexOf('success')>-1){
                        $("#replicaFolio").val(folioActual);
                    } else{
                        $('#mensaje').html(data);
                    }
                }
            });
        }        
    }
    BorrarValidacion();
}
//Mostrar Clientes
function mostrarRegistro(){
    xmlhttp= ComprobarNavegador();
    xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState===4 && xmlhttp.status===200){
        resultado.innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","../../core/models/servidorRegistro.php",true);
    xmlhttp.send();
}

function agregar(){
    var numeroBoletos = $("#noBoletos").val();
    var resultado;
    if(numeroBoletos>0){
        resultado = "<table align='center' width='100%' class='table table-hover' id='campos2'>";
        resultado += "<tr>";
                resultado += "<td>Nombre</td>";
                resultado += "<td>Folio</td>";
                resultado += "<td>Fecha Nacimiento</td>";
                resultado += "<td>Edad</td>";
            resultado += "</tr>";
        for (x=0; x<numeroBoletos; x++){
            resultado += "<tr>";
                resultado += "<td><input type='text' onkeypress='return BorrarValidacion()' class='form-control' name='nombre' id='n"+ x +"'></td>";
                resultado += "<td><input type='text' onkeypress='return BorrarValidacion()' class='form-control' name='folio' id='f"+ x +"'></td>";
                resultado += "<td><input type='date' onkeypress='return BorrarValidacion()' class='form-control' name='fechaNacimiento' id='fn"+ x +"'></td>";
                resultado += "<td><input type='text' class='form-control' onkeypress='return BorrarValidacion()' name='edad' id='e"+ x +"'></td>";
            resultado += "</tr>";
        }
        resultado += "</table>";
        document.getElementById("campos").innerHTML = resultado;
       } else{
            resultado = "";
            document.getElementById("campos").innerHTML = resultado;
       }
}
$(document).ready(function(){
    //Botón para registrar
    $("#registrar").on("click", function(){
        var resultado   = '';
        var correcto = false;
        var nombre      = $("#nombre").val();
        var apellidos   = $("#apellidos").val();
        var edad        = $("#edad").val();
        var correo      = $("#correo").val();
        var telefono    = $("#telefono").val();
        var escuela     = $("#escuela").val();
        var id_ciudad   = $("#txtciudad").val();
        var folio       = $("#folio").val();
        var noBoletos   = $("#noBoletos").val();
        var ArregloNombre = [];
        var ArregloFolio  = [];
        var ArregloFecha  = [];
        var ArregloEdad   = [];
        var equipo = $("#equipo").val();
        var lugar  = $("#id_lugar").val();
        var Validacion1 = false;
        var Validacion2 = false;
        var id;
        var indice=0;
        var continuacion=false;
        var envio=false;
        if(nombre!=''){
            if(edad!=''){
                if(apellidos!=''){
                    if(correo!=''){
                        correcto = VerificarCorreo(correo);
                        if(correcto){
                            if(telefono!=''){
                                if(escuela!=''){
                                    if(folio!=''){
                                       if(equipo!='null'){
                                           if(lugar!=''){
                                               if(id_ciudad!=''){
                                                   if(noBoletos=='0'||noBoletos==''){
                                                       //No hay validacion de campos extras
                                                       envio=true;

                                                      } else{
                                                        Validacion1=true;
                                                        //Validar campos extras
                                                        //Validar los campos nombres
                                                        $("input[name='nombre']").each(function(){
                                                        if($(this).val()!=''){
                                                              ArregloNombre.push($(this).val());
                                                              Validacion1=true;
                                                           } else{
                                                              resultado  ='<div class="alert alert-danger">';
                                                              resultado +='<strong>ERROR!</strong> Escriba el nombre';
                                                              resultado +='</div>';
                                                              document.getElementById("mensaje").innerHTML = resultado;
                                                              id="#n"+indice;
                                                              $(id).focus();
                                                              Validacion1=false;
                                                              return;
                                                           }
                                                         indice=indice+1;
                                                        });
                                                        indice=0;
                                                      }
                                               } else{
                                                     resultado  ='<div class="alert alert-danger">';
                                                      resultado +='<strong>ERROR!</strong> Escriba la ciudad del Estado';
                                                      resultado +='</div>';
                                                      document.getElementById("mensaje").innerHTML = resultado;
                                                      $("#txtciudad").focus();
                                               }
                                              } else{
                                                  resultado  ='<div class="alert alert-danger">';
                                                  resultado +='<strong>ERROR!</strong> Escriba el lugar de donde nos visita';
                                                  resultado +='</div>';
                                                  document.getElementById("mensaje").innerHTML = resultado;
                                                  $("#txtvisita").focus();
                                                  }
                                              } else{
                                                  resultado  ='<div class="alert alert-danger">';
                                                  resultado +='<strong>ERROR!</strong> Seleccione un equipo';
                                                  resultado +='</div>';
                                                  document.getElementById("mensaje").innerHTML = resultado;
                                                  $("#equipo").focus();
                                              }
                                       } else{
                                           resultado  ='<div class="alert alert-danger">';
                                           resultado +='<strong>ERROR!</strong>  Introduzca el folio del boleto ';
                                           resultado +='</div>';
                                           document.getElementById("mensaje").innerHTML = resultado;
                                           $("#folio").focus();
                                       }
                                } else{
                                    resultado  ='<div class="alert alert-danger">';
                                    resultado +='<strong>ERROR!</strong>Escuela/Equipo vacio';
                                    resultado +='</div>';
                                    document.getElementById("mensaje").innerHTML = resultado;
                                    $("#escuela").focus();
                                }
                               } else{
                                resultado  ='<div class="alert alert-danger">';
                                resultado +='<strong>ERROR!</strong>Teléfono vacio';
                                resultado +='</div>';
                                document.getElementById("mensaje").innerHTML = resultado;
                                $("#telefono").focus();
                               }
                       } else{
                        resultado  ='<div class="alert alert-danger">';
                        resultado +='<strong>ERROR!</strong> Correo invalido';
                        resultado +='</div>';
                        document.getElementById("mensaje").innerHTML = resultado;
                        $("#correo").focus();
                       }
                } else{
                   resultado  ='<div class="alert alert-danger">';
                   resultado +='<strong>ERROR!</strong> Correo vacío';
                   resultado +='</div>';
                   document.getElementById("mensaje").innerHTML = resultado;
                   $("#correo").focus();
                }
               } else{
                   resultado  ='<div class="alert alert-danger">';
                   resultado +='<strong>ERROR!</strong> Apellidos vacíos';
                   resultado +='</div>';
                   document.getElementById("mensaje").innerHTML = resultado;
                   $("#apellidos").focus();
               }
           } else{
                resultado  ='<div class="alert alert-danger">';
                resultado +='<strong>ERROR!</strong> Edad vacío';
                resultado +='</div>';
                document.getElementById("mensaje").innerHTML = resultado;
                $("#edad").focus();
           }
        } else{
            resultado  ='<div class="alert alert-danger">';
            resultado +='<strong>ERROR!</strong> Nombre vacío';
            resultado +='</div>';
            document.getElementById("mensaje").innerHTML = resultado;
            $("#nombre").focus();
        }
        
        if(Validacion1){
            $("input[name='folio']").each(function(){
                if($(this).val()!=''){
                    ArregloFolio.push($(this).val());
                    Validacion2=true;
                   } else{
                      resultado  ='<div class="alert alert-danger">';
                      resultado +='<strong>ERROR!</strong> Escriba el folio';
                      resultado +='</div>';
                      document.getElementById("mensaje").innerHTML = resultado;
                      id="#f"+indice;
                      $(id).focus();
                      Validacion2=false;
                      return;
                   }
                 indice=indice+1;
            });
            $("input[name='edad']").each(function(){
                if($(this).val()!=''){
                    ArregloEdad.push($(this).val());
                    Validacion2=true;
                   } else{
                      resultado  ='<div class="alert alert-danger">';
                      resultado +='<strong>ERROR!</strong> Escriba la edad';
                      resultado +='</div>';
                      document.getElementById("mensaje").innerHTML = resultado;
                      Validacion2=false;
                      return;
                   }
                 indice=indice+1;
            });
        }
        indice=0;
        if(Validacion2){
            $("input[name='fechaNacimiento']").each(function(){
                if($(this).val()!=''){
                      ArregloFecha.push($(this).val());
                      continuacion=true;
                   } else{
                      resultado  ='<div class="alert alert-danger">';
                      resultado +='<strong>ERROR!</strong> Seleccione una fecha';
                      resultado +='</div>';
                      document.getElementById("mensaje").innerHTML = resultado;
                      id="#fn"+indice;
                      $(id).focus();
                       Validacion2=false;
                       continuacion=false;
                       return;
                   }
                 indice=indice+1;
            });
        }
        indice=0;
        if(continuacion){
            for (x=0; x<ArregloFolio.length; x++){
                for (y=0; y<ArregloFolio.length; y++){
                    if(ArregloFolio[x]==ArregloFolio[y] && x!=y || ArregloFolio[x]==folio){
                        resultado  ='<div class="alert alert-danger">';
                        resultado +='<strong>ERROR!</strong> Folios Repetidos';
                        resultado +='</div>';
                        document.getElementById("mensaje").innerHTML = resultado;
                        id="#f"+y;
                        $(id).focus();
                        envio=false;
                        return;
                       } else{
                        envio=true;
                       }
                }
            }
        }
        var parametros ={
            "nombre"   : nombre,
            "apellidos" : apellidos,
            "correo"    : correo,
            "folio"     : folio,
            "ArregloFolio" : ArregloFolio,
            "ArregloFecha" : ArregloFecha,
            "ArregloNombre": ArregloNombre,
            "equipo"        : equipo,
            "lugar"         : lugar,
            "edad"          : edad,
            "telefono"      : telefono,
            "escuela"       : escuela,
            "ArregloEdad"   : ArregloEdad,
            "id_ciudad"     : id_ciudad
        };
        
        if(envio){
             $.ajax({
                type : 'POST',
                data : parametros,
                url  : '../models/registrar.php',
                success : function(data){
                    if(data.indexOf('success')>-1){
                        alert("Gracias por su registro. Se le ha enviado un correo electronico.");
                        window.location='registro.php';
                       } else{
                          $('#mensaje').html(data);
                       }
                }
            });        
        }
               
    });
    
    //login
    //Botón validar login
    $("#iniciar").on('click', function(){
        var user = $("#user").val();
        var pass = $("#pass").val();
        var parametros = {
            "user": user,
            "pass" : pass
        }
        if(user!='' && pass!=''){
            $.ajax({
                type: 'POST',
                data: parametros,
                url : '../models/validar.php',
                success: function(data){
                    if(data.indexOf('success')>-1){ 
                        window.location='verRegistro.php';
                    } else{
                        resultado  ='<div class="alert alert-danger">';
                        resultado +='<strong>ERROR!</strong> Credenciales incorrectas';
                        resultado +='</div>';
                        document.getElementById("mensaje").innerHTML = resultado;
                    }
                }
            });
           } else{
               resultado ='<div class="alert alert-danger">';
               resultado +='<strong>ERROR!</strong> Campos vacíos';
               resultado +='</div>';
               document.getElementById("mensaje").innerHTML = resultado;
           }
    });
});