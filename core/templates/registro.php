<?php
    include_once('../models/obtenerVisitas.php');
    include_once('../models/obtenerEquipos.php');
    include_once('../models/servidorEstados.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/estiloRegistro.css"/>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/bootstrap.min.css">
        <script type="text/x-javascript" src="../../styles/js/jquery-1.12.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/jquery-ui.css">
        <script type="text/javascript" src="../../styles/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../styles/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../styles/js/funciones.js"></script>
        <title>Registro</title>
    </head>
    <script>
        $(document).ready(function () {
			var items = <?= json_encode($array1) ?>

			$("#txtvisita").autocomplete({
				source: items,
				select: function (event, item) {
					var params = {
						clave: item.item.value
					};
					$.get("../models/completarEstado.php", params, function (response) {
						var json2 = JSON.parse(response);
                        if (json2.status == 200){
                            $("#id_lugar").val(json2.id_lugar);
						}
					}); // ajax
				}
			});
		});
    </script>
    <body>
        <div class="contenido">
            <div id="imagen"><img src="../../styles/images/logo1.png"></div>
            <div class="texto" id="textoTitulo">Registro</div><br>
            <div id="mensaje"></div>
            <div class="texto" id="textoNombre">Nombre:</div>
            <div id="divNombre"><input class="form-control" type="text" id="nombre" maxlength="30" onkeypress="return BorrarValidacion()" title="Escriba su nombre"></div>
            <div class="texto" id="textoEdad">Edad:</div>
            <div id="divEdad"><input class="form-control" type="text" id="edad" maxlength="2" onkeypress="BorrarValidacion(); return solonumeros(event);" title="Escriba su edad"></div>
            <div class="texto" id="textoApellidos">Apellidos:</div>
            <div id="divApellidos"><input class="form-control" type="text" id="apellidos" maxlength="50" onkeypress="return BorrarValidacion()" title="Escriba sus Apellidos, empezando por el apellido paterno"></div>
            <div class="texto" id="textoCorreo">Correo:</div>
            <div id="divCorreo"><input class="form-control" type="text" id="correo" maxlength="70" onkeypress="return BorrarValidacion()" title="Escriba su correo electronico"></div>
            <div class="texto" id="textoTelefono">Teléfono:</div>
            <div id="divTelefono"><input class="form-control" type="text" id="telefono" maxlength="10" onkeypress="BorrarValidacion(); return solonumeros(event);" title="Escriba su número de teléfono"></div>
            <div class="texto" id="textoEscuela">Escuela/Equipo:</div>
            <div id="divEscuela"><input class="form-control" type="text" id="escuela" maxlength="70" onkeypress="return BorrarValidacion()"></div>
            <div class="texto" id="textoFolio">Folio del boleto:</div>
            <div id="divFolio"><input class="form-control" type="text" id="folio" maxlength="18" onkeyup="return BorrarValidacion();" title="Escriba el folio del boleto adquirido"></div>
            <div class="texto" id="textoNoBoletos"># de menores:</div>
            <div id="divNoBoletos"><input class="form-control" type="text" id="noBoletos" onkeyup="return agregar();" onkeypress="return solonumeros(event)" title="Número de menores"></div>
            <div id="campos"></div>
            <div class="texto" id="textoEquipo">Equipo de NFL:</div>
            <div class="divEquipo"><select id="equipo">
            <option value="null">--Seleccione una opcion--</option>    
            <?php
                while($datosEquipo=$conexion->recorrer($equipos)){
            ?>
            <option value="<?php echo $datosEquipo[0] ?>"><?php echo $datosEquipo[1] ?></option>
            <?php } ?>  
            </select></div>
            <div class="texto" id="textoLugar">Lugar de dónde nos visita:</div>
            <div id="divLugar"><input class="form-control" type="text" id="txtvisita" maxlength="80" onkeypress="return BorrarValidacion()" title="Escriba el lugar de dónde nos visita"><input hidden="hidden" type="text" id="id_lugar"> </div>
            <div class="texto" id="textoCiudad">Ciudad:</div>
            <div id="divCiudad"><input class="form-control" type="text" id="txtciudad" maxlength="70" onkeypress="return BorrarValidacion()" title="Escriba la ciudad"><input hidden="hidden" type="text" id="id_ciudad"> </div>
             <div class="botones">
                <button type="button" class="btn btn-success" id="registrar">Registrar</button>
            </div>
            
            <div id="clausulas">
            <p>Con fundamento en los artículos 15 y 16 de la Ley Federal de Protección de Datos Personales en Posesión de Particulares hacemos de su conocimiento que ESTRELLAS DEL FUTURO A.C., con domicilio en Anillo vial fray junípero sierra No.3000, Querétaro es responsable de recabar sus datos personales, del uso que se le dé a los mismos y de su protección. Su información personal será utilizada para las siguientes finalidades: proveer los servicios y productos que ha solicitado; notificarle sobre nuevos servicios o productos que tengan relación con los ya contratados o adquiridos; comunicarle sobre cambios en los mismos; elaborar estudios y programas que son necesarios para determinar hábitos de consumo; realizar evaluaciones periódicas de nuestros productos y servicios a efecto de mejorar la calidad de los mismos; evaluar la calidad del servicio que brindamos, y en general, para dar cumplimiento a las obligaciones que hemos contraído con usted. Para las finalidades antes mencionadas, requerimos obtener los siguientes datos personales: Nombre completo, Edad, Correo electrónico, Es importante informarle que usted tiene derecho al Acceso, Rectificación y Cancelación de sus datos personales, a Oponerse al tratamiento de los mismos o a revocar el consentimiento que para dicho fin nos haya otorgado. Para ello, es necesario que envíe la solicitud en los términos que marca la Ley en su Art. 29 al responsable de nuestro Departamento de Protección de Datos al teléfono 4422543567 o vía correo electrónico a info@estrellasdelfuturo.com.mx, el cual solicitamos confirme vía telefónica para garantizar su correcta recepción. en caso de que no desee de recibir mensajes promocionales de nuestra parte, puede enviarnos su solicitud por medio de la dirección electrónica:</p>
            </div>
            
        </div>
        
    </body>
</html>