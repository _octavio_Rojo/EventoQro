<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/bootstrap.min.css">
        <script type="text/javascript" src="../../styles/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../../styles/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../styles/js/funciones.js"></script>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/estiloLogin.css">
        <title>Login</title>
    </head>
    <body>
        <div class="container">
        <center>
              <div class="form-signin">
                <h2 class="form-signin-heading">Iniciar sesión</h2>
                <div id="mensaje"></div>
                <input id="user" class="form-control" placeholder="Usuario" autofocus="" onkeypress="return BorrarValidacion()" type="text">
                <input id="pass" class="form-control" placeholder="Contraseña" type="password" onkeypress="return BorrarValidacion()">
                <button class="btn btn-primary btn-block" type="button" id="iniciar">Iniciar sesión</button>
            </div>
        </center>
        </div>
    </body>
</html>