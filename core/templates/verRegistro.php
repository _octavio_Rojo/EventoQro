<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="../../styles/ccs/bootstrap.css"/>
        <script type="text/javascript" src="../../styles/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../../styles/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../styles/js/funciones.js"></script>
        <title>Registros</title>
    </head>
    <script language="javascript">
    $(document).ready(function() {
        $("#botonExcel").click(function(event) {
            $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
            $("#FormularioExportacion").submit();
    });
    });
    </script>
    <style type="text/css">
        #botonExcel{float: right;}
    </style>
    <body onload="mostrarRegistro()">
        <form action="../models/ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
            <button id="botonExcel" class="btn btn-success">Exportar a Excel</button>
            <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
        </form>
         <div><center><h2>Registros</h2></center></div>
        <div class="catalogo">
            <div id="mostrar"></div>
        </div>
    </body>
    <script>
        var resultado=document.getElementById("mostrar");
    </script>
</html>