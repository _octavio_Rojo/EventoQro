<?php
    require_once('class.Conexion.php');
    require "../../libs/phpqrcode/qrlib.php";  //Incluimos la libreria para generacion de qr
    $dir = 'codigos/'; //Declarmos la ruta donde se guardaran los códigos
    if (!file_exists($dir)){//Si no existe la carpeta la creamos
        mkdir($dir);
    }
    $nombre     = $_POST['nombre'];
    $apellidos  = $_POST['apellidos'];
    $correo     = $_POST['correo'];
    $folio      = $_POST['folio'];
    $edad      = $_POST['edad'];
    $telefono      = $_POST['telefono'];
    $escuela      = $_POST['escuela'];
    $id_ciudad      = $_POST['id_ciudad'];
    $ArregloFolio = json_encode($_POST['ArregloFolio']);
    $ArregloFecha = json_encode($_POST['ArregloFecha']);
    $ArregloEdad = json_encode($_POST['ArregloEdad']);
    $ArregloNombre = json_encode($_POST['ArregloNombre']);
    $ArregloFolio = json_decode($ArregloFolio);
    $ArregloFecha = json_decode($ArregloFecha);
    $ArregloEdad = json_decode($ArregloEdad);
    $ArregloNombre = json_decode($ArregloNombre);
    $id_Equipo  = $_POST['equipo'];
    $lugar      = $_POST['lugar'];
    $conexion   = new Conexion();
    $tamanio = count($ArregloFolio);
    $duplicidad=false;
    //Consulta de folio repetidos en la base de datos
    $consulta_folio = mysqli_query($conexion,"SELECT * FROM registro_tb WHERE folio='$folio';");
    $consulta_folio = $conexion->rows($consulta_folio);
    if($consulta_folio>0){
        echo '<div class="alert alert-danger">';
        echo '<strong>ERROR!</strong> El folio '.$folio.' ya fue registrado';
        echo '</div>';
        $duplicidad = true;
    } else{
        for($i=0; $i<$tamanio; $i++){
            $consulta_folio = mysqli_query($conexion,"SELECT * FROM registro_tb WHERE folio='$ArregloFolio[$i]';");
            $consulta_folio = $conexion->rows($consulta_folio);
            if($consulta_folio>0){
                echo '<div class="alert alert-danger">';
                echo '<strong>ERROR!</strong> El folio '.$ArregloFolio[$i].' ya fue registrado';
                echo '</div>';
                $duplicidad = true;
                return;
            } else{
                $duplicidad = false;
            }
        }
    }
    $obtenerFecha = date("Y-m-d H:i:s");
    if(!$duplicidad){
        echo "success";
        mysqli_query($conexion, "INSERT INTO registro_tb(nombre, apellidos, correo, folio, edad, telefono, escuela, ciudad, id_equipoNFL, id_lugar, fecha_hora) VALUES ('$nombre', '$apellidos', '$correo', '$folio', '$edad', '$telefono', '$escuela', '$id_ciudad', $id_Equipo, '$lugar', '$obtenerFecha');");
        for($i=0; $i<$tamanio; $i++){
            mysqli_query($conexion, "INSERT INTO registro_tb(nombre, folio, fecha_nacimiento,edad) VALUES ('$ArregloNombre[$i]', '$ArregloFolio[$i]', '$ArregloFecha[$i]','$ArregloEdad[$i]');");
        }
        //Configuración del codigo QR
        $filename = $dir.$obtenerFecha.$nombre.'.png'; //Declaramos la ruta y nombre del archivo a generar
	    $tamaño = 10; //Tamaño de Pixel
        $level = 'L'; //Precisión Baja
        $framSize = 3; //Tamaño en blanco
        $contenido = $folio;
        QRcode::png($contenido, $filename, $level, $tamaño, $framSize);
        //Enviar el Correo electronico
        $asunto = "Código para FOOTBALL CAMP";
        $mail = "info@estrellasdelfuturo.com.mx";
        $nameFile = $filename; //Ruta del archivo a enviar
        $sizeFile = filesize($nameFile); //Tamaño del archivo a enviar
        $correoDestino = $correo;
        // -> mensaje en formato Multipart MIME
        $cabecera = "MIME-VERSION: 1.0\r\n";
        $cabecera .= "Content-type: multipart/mixed;";
        $cabecera .="boundary=\"=C=T=E=C=\"\r\n";
        $cabecera .= "From: {$mail}";
        $cuerpo ="";

        // -> segunda parte del mensaje (archivo adjunto)
        // -> encabezado de la parte
        $cuerpo .= "--=C=T=E=C=\r\n";
        $cuerpo .= "Content-Type: application/zip; ";
        $cuerpo .= "name=" . $nameFile . "\r\n";
        $cuerpo .= "Content-Transfer-Encoding: base64\r\n";
        $cuerpo .= "Content-Disposition: attachment; ";
        $cuerpo .= "filename=" . $nameFile . "\r\n";
        $cuerpo .= "\r\n"; // línea vacía

        $fp = fopen($nameFile, "rb");
        $file = fread($fp, $sizeFile);
        $file = chunk_split(base64_encode($file));

        $cuerpo .= "$file\r\n";
        $cuerpo .= "\r\n"; // línea vacía
        // Delimitador de final del mensaje.
        $cuerpo .= "--=C=T=E=C=--\r\n";
        $enviar = mail($correoDestino, $asunto, $cuerpo, $cabecera);        
    }
    
        
?>