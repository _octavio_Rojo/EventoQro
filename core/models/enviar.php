<?php
    require "../../libs/phpqrcode/qrlib.php";  //Incluimos la libreria para generacion de qr
    $dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);
	
        //Declaramos la ruta y nombre del archivo a generar
	$filename = $dir.'QR.png';
    //Parametros de Condiguración
	
	$tamaño = 10; //Tamaño de Pixel
	$level = 'L'; //Precisión Baja
	$framSize = 3; //Tamaño en blanco
    $contenido = "?re=".$rfcEmpresa."&rr=".$rfcCliente."&tt=000".$T."000&id=".$folioFiscal;
    QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
?>