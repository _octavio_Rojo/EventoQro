/*Se crea una base de datos llamada eventoQro_bd
    CUIDADO
    Al ejecutar este script se reescriben los datos
*/ 
DROP DATABASE IF EXISTS eventoQro_db;
CREATE DATABASE eventoQro_db;
USE eventoQro_db;
                             
CREATE TABLE equipoNFL_tb(id_equipoNFL int not null auto_increment,
                             equipoNFL varchar(50),
                             PRIMARY KEY(id_equipoNFL));
                             
CREATE TABLE lugar_tb(id_lugar int not null auto_increment,
                             lugar varchar(50),
                             PRIMARY KEY(id_lugar));
                             
CREATE TABLE login_tb(id_login int not null auto_increment,
                             usuario varchar(50),
                             pass varchar(50),
                             PRIMARY KEY(id_login));

CREATE TABLE registro_tb(id_registro int not null auto_increment,
                        nombre varchar(30),
                        apellidos varchar(50),
                        edad int,
                        correo varchar(70),
                        telefono varchar(10),
                        folio varchar(18),
                        escuela varchar(70),
                        id_equipoNFL int,
                        FOREIGN KEY (id_equipoNFL) REFERENCES equipoNFL_tb(id_equipoNFL),
                        id_lugar int,
                        FOREIGN KEY(id_lugar) REFERENCES lugar_tb(id_lugar),
                        ciudad varchar(70),
                        fecha_nacimiento date,
                        fecha_hora datetime,
                        PRIMARY KEY(id_registro));

INSERT INTO login_tb(usuario, pass) VALUES ('administrador', '9dbf7c1488382487931d10235fc84a74bff5d2f4');

INSERT INTO equipoNFL_tb(equipoNFL) VALUES ('Arizona Cardinals'), ('Atlanta Falcons'),('Baltimore Ravens'), ('Buffalo Bills'), ('Carolina Panthers'), ('Chicago Bears'), ('Cincinnati Bengals'), ('Cleveland Browns'), ('Dallas Cowboys'), ('Denver Broncos'), ('Detroit Lions'), ('Green Bay Packers'), ('Houston Texans'), ('Indianapolis Colts'), ('Jacksonville Jaguars'), ('Kansas Cuty Chiefs'), ('Los Angekes Chargers'), ('Los Angeles Rams'), ('Miami Dolphins'), ('Minnesota Vikings'), ('New England Patriots'), ('New Orleans Saints'), ('New York Giants'), ('New York Jets'),('Oakland Raiders'), ('Philadelphia Eagles'), ('Pittsburgh Steelers'), ('San Francisco 49ers'), ('Seattle Swahawks'), ('Tampa Bay Buccaneers'), ('Tennessee Titans'), ('Washington Redskins');

INSERT INTO lugar_tb(lugar) VALUES ('Aguascalientes'), ('Baja California'), ('Baja California Sur'), ('Campeche'), ('Coahuila'), ('Colima'), ('Chiapas'),('Chihuahua'),(' Distrito Federal '), ('Durango'), ('Guanajuato'), ('Guerrero'), ('Hidalgo'), ('Jalisco'), ('Mexico'), ('Michoacan'), ('Morelos'), ('Nayarit'), ('Nuevo León'), ('Oaxaca'), ('Puebla'), ('Queretaro'), ('Quintana Roo'), ('San Luis Potosi'), ('Sinaloa'), ('Sonora'), ('Tabasco'), ('Tamaulipas'), ('Tlaxcala'), ('Veracruz'), ('Yucatan'), ('Zacatecas');
                        
GRANT ALL PRIVILEGES ON eventoQro_db .* to eventoQro_user@localhost IDENTIFIED BY 'pass_eventoQro';